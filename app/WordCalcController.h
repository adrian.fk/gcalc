//
// Created by Adrian Karlsen on 13/02/2021.
//

#ifndef GEOMATRADECRYPTER_WORDCALCCONTROLLER_H
#define GEOMATRADECRYPTER_WORDCALCCONTROLLER_H

#include "controller/WordCalcPresenter.h"


typedef struct Class_WordCalcController WordCalcController;

WordCalcController* WORDCALC_Controller_new();

void WORDCALC_Controller_init(WordCalcController* controller);

void WORDCALC_Controller_free(WordCalcController* controller);

#endif //GEOMATRADECRYPTER_WORDCALCCONTROLLER_H
