#include <stdio.h>
#include "Utils.h"

void printArrayListItem(void* item) {
    int* tstVal = (int *) item;
    printf("\nArrayList val: %d", *tstVal);
}

int main() {
    printf("Hello, World!\n");

    LinkedList* ll = LINKED_LIST_new();
    LinkedList* ll2 = LINKED_LIST_new();

    int tstArr[10] = {1, 2, 3, 4, 5, 6 , 7, 8, 9, 0};


    


    String* test = STRING_new();
    STRING_set(test, "\nthis is a test with String object");

    LINKED_LIST_init(ll, STRING_getSizeOf());


    LINKED_LIST_push(ll, test);

    String* testOut = (String*) LINKED_LIST_get(ll, 0);

    printf("%s", STRING_getCharArr(testOut));



    LINKED_LIST_init(ll2, sizeof(int));

    for (int i = 0; i < 10; i++) {
        LINKED_LIST_push(ll2, &tstArr[i]);
    }

    int* tstVal3 = (int*) LINKED_LIST_get(ll2, 9);

    printf("\n\nInt Array val 3: %d Linked List val 3: %d", tstArr[9], *tstVal3);


    ArrayList* arrayList = ARRAY_LIST_new();
    ARRAY_LIST_init(arrayList, sizeof(int));

    for (int i = 0; i < 10; i++) {
        ARRAY_LIST_add(arrayList, &tstArr[i]);
    }

    ARRAY_LIST_forEach(arrayList, &printArrayListItem);

    Map* map = MAP_new();

    String* mapTstString = STRING_init("This is a map item");
    String* mapTstString2 = STRING_init("wtfLoLOLOLllOlll");

    MAP_init(map, STRING_getSizeOf());
    MAP_add(map, "test", mapTstString);
    MAP_add(map, "test", mapTstString2);
    MAP_add(map, "test2", mapTstString2);


    String* mapOut = (String*) MAP_get(map, "test2");
    printf("\n\n%s", STRING_getCharArr(mapOut));


    return 0;

}
