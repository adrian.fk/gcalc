//
// Created by adria on 03/03/2020.
//

#include <stdlib.h>
#include <stdio.h>
#include "SimplefiedStrings.h"

#define STRING_NULL_CHAR '\0'

#define INPUT_BUFFER_SIZE 100
#define DEFAULT_BUFFER_SIZE 1024
#define TRUE 1
#define FALSE 0

struct string {
    int length;
    char* string;
};

char STRING_getCharAtIndex(const String* string, const int index);
int charArrLen(const char* input);



String* STRING_new() {
    String *out = (String*)malloc(sizeof(String));
    if (NULL != out) {
        out->string = (char*) malloc(sizeof(char));
        if (NULL != out->string) {
            out->string[0] = '\0';
            STRING_set(out, "");
        }
    }

    return out;
}



String* STRING_init(char* input) {
    String *out = STRING_new();
    if (NULL != input) STRING_set(out, input);

    return out;
}

void STRING_appendChar(String* string, char input) {
    string->length += 1;
    string->string = (char*) realloc(string->string, (string->length + 1)*sizeof(char));
    if(string->string) {
        string->string[string->length - 1] = input;
        string->string[string->length] = '\0';
    }
}

void STRING_append(String* string, char input[]) {
    int inputLength = charArrLen(input);
    size_t totalNewLength = (size_t) string->length + inputLength;
    string->string = (char*) realloc(string->string, totalNewLength);
    if(string->string) {
        for (int i = 0; i < inputLength; i++) {
            STRING_appendChar(string, input[i]);
        }
    }
    else {
        //handle error
    }
}

int charArrLen(const char* input) {
    int i;
    if (input[0] == STRING_NULL_CHAR) {
        return 0;
    } else {
        for (i = 0; STRING_NULL_CHAR != input[i]; i++) {}
        return i;
    }
}

int STRING_length(String* string) {
    return charArrLen(string->string);
}

void STRING_set(String* string, char input[]) {
    int i;
    int length = charArrLen(input);

    string->string = (char*) realloc(string->string, length * sizeof(char) + 1);
    if(string->string) {
        string->length = length;
        for (i = 0; i < length; i++) {
            string->string[i] = input[i];
        }
        string->string[i] = '\0';
    }
    else {
        //handle error
    }
}

char* STRING_getCharArr(String* strObj) {
    return strObj->string;
}

size_t STRING_getSizeOf() {
    return sizeof(String);
}

void STRING_getInput(String* _dest) {
    char buffer[INPUT_BUFFER_SIZE] = {'\0'};
    fgets(buffer, INPUT_BUFFER_SIZE, stdin);
    if ('\n' == buffer[strlen(buffer)-1]) buffer[strlen(buffer)-1] = '\0';
    STRING_set(_dest, buffer);
}

char STRING_getChar(String* strObj, int index) {
    char out = '\0';
    if(index < strObj->length) {
        out = strObj->string[index];
    }
    return out;
}

void STRING_destroy(String* _dest) {
    free(_dest->string);
    free(_dest);
}

int STRING_getLength(String* string) {
    return string->length;
}

int STRING_equals(String* stringA, String* stringB) {
    int strCmpResult = -1;
    if (NULL != stringA && NULL != stringB && stringA->string != NULL && stringB->string != NULL && stringA->length == stringB->length) strCmpResult = strcmp(stringA->string, stringB->string);
    if (strCmpResult == 0) return TRUE;
    return FALSE;
}

void STRING_setString(String* stringObj, String* newString) {
    STRING_set(stringObj, newString->string);
}

int STRING_atoi(String* stringObj) {
    return atoi(stringObj->string);
}

char STRING_getCharAtIndex(const String* string, const int index) {
    return string->string[index];
}

void STRING_subString(const String* string, String* out, int startIndex, int endIndex) {
    if (NULL != out && string != NULL && startIndex < string->length) {
        String* tmp = STRING_init("");
        for (int i = startIndex; i <= endIndex; ++i) {
            if (i < string->length) STRING_appendChar(tmp, STRING_getCharAtIndex(string, i));
        }
        STRING_set(out, STRING_getCharArr(tmp));
        STRING_destroy(tmp);
    }
}

String* STRING_subString_new(const String* string, int startIndex, int endIndex) {
    String* out = STRING_new();
    STRING_subString(string, out, startIndex, endIndex);
    return out;
}

void STRING_convert_intToString(String* out, int in) {
    char buf[DEFAULT_BUFFER_SIZE] = {'\0'};
    sprintf(buf, "%d", in);
    STRING_set(out, buf);
}

