//
// Created by Adrian Karlsen on 20/01/2021.
//

#include "../../SimpleMenuContainer.h"

struct SimpleMenuContainer__prototype {
    String* exitStatement;
    String* menuTitle;
    String* errorMessage;
    Array menuItems;
    int longestTextLength;
};

SimpleMenuContainer* SIMPLE_MENU_CONTAINER_init() {
    SimpleMenuContainer* o = (SimpleMenuContainer*) malloc(sizeof(struct SimpleMenuContainer__prototype));
    if (NULL != o) {
        o->exitStatement = STRING_new();
        o->menuTitle = STRING_new();
        o->errorMessage = STRING_new();
        o->menuItems = ARRAY_constructor(SIMPLE_MENU_ENTRY_objectSize());
        o->longestTextLength = 0;

        //Default exit statement
        STRING_set(o->exitStatement, "exit");

        //Default error message
        STRING_set(o->errorMessage, "Invalid menu option.. Try again.");
    }

    return o;
}

void SIMPLE_MENU_CONTAINER_addMenuEntry(SimpleMenuContainer* containerObj, SimpleMenuEntry* entry) {
    ARRAY_add(containerObj->menuItems, entry);

    int entryTextLength = STRING_length(SIMPLE_MENU_ENTRY_getText(entry));
    if (containerObj->longestTextLength < entryTextLength) containerObj->longestTextLength = entryTextLength;
}

void SIMPLE_MENU_CONTAINER_removeEntryById(SimpleMenuContainer* container, String* id) {
    int i;
    for (i = 0; i < ARRAY_getLength(container->menuItems); i++) {
        SimpleMenuEntry* currEntry = ARRAY_get(container->menuItems, i);
        if (STRING_equals(SIMPLE_MENU_ENTRY_getId(currEntry), id)) break;
    }
    SIMPLE_MENU_CONTAINER_removeEntryByIndex(container, i);
}

void SIMPLE_MENU_CONTAINER_removeEntryByIndex(SimpleMenuContainer* container, int index) {
    ARRAY_remove(container->menuItems, index);
}

int SIMPLE_MENU_CONTAINER_containerLength(SimpleMenuContainer* container) {
    return ARRAY_getLength(container->menuItems);
}

SimpleMenuEntry* SIMPLE_MENU_CONTAINER_get(const SimpleMenuContainer* container, int index) {
    if (NULL != ARRAY_get(container->menuItems, index)) return ARRAY_get(container->menuItems, index);
    //else return NULL
    return NULL;
}

SimpleMenuEntry* SIMPLE_MENU_CONTAINER_getById(const SimpleMenuContainer* containerObj, String* id) {
    for (int i = 0; i < ARRAY_getLength(containerObj->menuItems); i++) {
        SimpleMenuEntry* currEntry = ARRAY_get(containerObj->menuItems, i);
        if (STRING_equals(SIMPLE_MENU_ENTRY_getId(currEntry), id)) return currEntry;
    }
    //If not found, return NULL
    return NULL;
}

String* SIMPLE_MENU_CONTAINER_getMenuTitle(SimpleMenuContainer* container) {
    return container->menuTitle;
}

void SIMPLE_MENU_CONTAINER_setMenuTitle(SimpleMenuContainer* container, String* title) {
    STRING_setString(container->menuTitle, title);
}

Boolean SIMPLE_MENU_CONTAINER_containsId(SimpleMenuContainer* container, String* id) {
    Boolean containsId = FALSE;
    for (int i = 0; i < ARRAY_getLength(container->menuItems); i++) {
        SimpleMenuEntry* target = ARRAY_get(container->menuItems, i);
        if (STRING_equals(id, SIMPLE_MENU_ENTRY_getId(target))) containsId = TRUE;
    }
    return containsId;
}

int SIMPLE_MENU_CONTAINER_getLongestEntryLength(SimpleMenuContainer* container) {
    return container->longestTextLength;
}

void SIMPLE_MENU_CONTAINER_setExitStatement(SimpleMenuContainer* container, String* exitStatement) {
    STRING_setString(container->exitStatement, exitStatement);
}

String* SIMPLE_MENU_CONTAINER_getExitStatement(const SimpleMenuContainer* container) {
    return container->exitStatement;
}

void SIMPLE_MENU_CONTAINER_setErrorMsg(SimpleMenuContainer* container, String* errorMsg) {
    if (NULL != container) STRING_setString(container->errorMessage, errorMsg);
}

String* SIMPLE_MENU_CONTAINER_getErrorMsg(SimpleMenuContainer* container) {
    return container->errorMessage;
}

void SIMPLE_MENU_CONTAINER_destroy(SimpleMenuContainer* container) {
    STRING_destroy(container->exitStatement);
    STRING_destroy(container->menuTitle);
    STRING_destroy(container->errorMessage);
    ARRAY_destroy(container->menuItems);
}
