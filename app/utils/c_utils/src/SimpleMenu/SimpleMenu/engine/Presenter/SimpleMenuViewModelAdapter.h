//
// Created by Adrian Karlsen on 21/01/2021.
//

#ifndef C_SIMPLEMENU_SIMPLEMENUVIEWMODELADAPTER_H
#define C_SIMPLEMENU_SIMPLEMENUVIEWMODELADAPTER_H

#include "SimpleMenuViewModel.h"
#include "../SimpleMenuEntry.h"

SimpleMenuViewModel* SIMPLE_MENU_VIEW_MODEL_ADAPTER_mapToViewModel(SimpleMenuEntry* menuEntry);

SimpleMenuEntry* SIMPLE_MENU_VIEW_MODEL_ADAPTER_mapToMenuEntry(SimpleMenuViewModel* viewModel);

#endif //C_SIMPLEMENU_SIMPLEMENUVIEWMODELADAPTER_H
