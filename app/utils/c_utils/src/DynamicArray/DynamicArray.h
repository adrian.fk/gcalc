#ifndef P2_ADRIAN_FALCH_DYNAMICARRAY_H
#define P2_ADRIAN_FALCH_DYNAMICARRAY_H

#endif //P2_ADRIAN_FALCH_DYNAMICARRAY_H

#include <stdlib.h>
#include <memory.h>


//ERROR DEFINITIONS
#define ARRAY_ERROR_MALLOC 3
#define ARRAY_ERROR_MALLOC_MEMBER 4
#define ARRAY_ERROR_SET_OOB 5
#define ARRAY_ERROR_GET_OOB 6
#define ARRAY_ERROR_GET_SORTABLE_OOB 7
#define ARRAY_ERROR_NO_SORATABLE_DATA 8

typedef struct ArrayPrototype* Array;

Array ARRAY_constructor(size_t _elementSize);

int ARRAY_getError(Array arr);

void ARRAY_setErrorHandled(Array arr);

void ARRAY_swap(Array arrObj, int xIndex, int yIndex);

void* ARRAY_get(Array arrObj, int index);

float ARRAY_getSortable(Array arrObj, int index);

void ARRAY_setAllocationSize(Array arr, int allocationSize);

void ARRAY_set(Array arrObj, int index, void* input);

void ARRAY_add(Array arrObj, void* input);

void ARRAY_setSortable(struct ArrayPrototype *arrObj, int index, float sortable);

int ARRAY_getLength(Array arrObj);

void ARRAY_remove(Array array, int index);

void ARRAY_destroy(Array arrObj);