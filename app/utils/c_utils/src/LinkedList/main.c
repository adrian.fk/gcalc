#include <stdio.h>
#include <strings.h>
#include "LinkedList/LinkedList.h"
#include "SimplefiedStrings/SimplefiedStrings.h"

int main() {
    printf("Hello, World!\n");

    LinkedList* ll = LINKED_LIST_new();
    LinkedList* ll2 = LINKED_LIST_new();

    int tstArr[10] = {1, 2, 3, 4, 5, 6 , 7, 8, 9, 0};







    String* test = STRING_init();
    STRING_set(test, "\nthis is a test with String object");

    LINKED_LIST_init(ll, STRING_getSizeOf());


    LINKED_LIST_push(ll, test);

    String* testOut = (String*) LINKED_LIST_get(ll, 0);

    printf("%s", STRING_getCharArr(testOut));



    LINKED_LIST_init(ll2, sizeof(int));

    for (int i = 0; i < 10; i++) {
        LINKED_LIST_push(ll2, &tstArr[i]);
    }

    int* tstVal3 = (int*) LINKED_LIST_get(ll2, 2);

    printf("\n\nInt Array val 3: %d Linked List val 3: %d", tstArr[2], *tstVal3);


    return 0;

}
