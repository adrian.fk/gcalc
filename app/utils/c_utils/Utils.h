//
// Created by Adrian Karlsen on 30/01/2021.
//

#ifndef C_FALCONUTILS_UTILS_H
#define C_FALCONUTILS_UTILS_H



#include "UtilsConstants.h"
#include "src/ArrayList/ArrayList.h"
#include "src/DynamicArray/DynamicArray.h"
#include "src/LinkedList/LinkedList.h"
#include "src/SimplefiedStrings/SimplefiedStrings.h"
#include "src/SimpleMenu/SimpleMenu.h"
#include "src/Map/Map.h"





#endif //C_FALCONUTILS_UTILS_H
