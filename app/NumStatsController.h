//
// Created by Adrian Karlsen on 12/02/2021.
//

#ifndef GEOMATRADECRYPTER_NUMSTATSCONTROLLER_H
#define GEOMATRADECRYPTER_NUMSTATSCONTROLLER_H

#include "controller/NumStatsPresenter.h"
#include "controller/DataLinks.h"

typedef struct Class_NumStatsController NumStatsController;

NumStatsController* NUMSTATS_Controller_new();

void NUMSTATS_Controller_init(NumStatsController* controller);

#endif //GEOMATRADECRYPTER_NUMSTATSCONTROLLER_H
