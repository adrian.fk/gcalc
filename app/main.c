#include <stdio.h>

#include "utils/c_utils/Utils.h"
#include "PreprocessController.h"
#include "MainMenuController.h"

int main() {

    void* menu = MAIN_MENU_new();
    MAIN_MENU_Controller_init((MainMenuController*) menu);
    
    /*
    //Attempt loading, and parse text file if bin file doesn't exist.
    PreprocessController* c = PREPROCESS_Controller_new();
    if (!PREPROCESS_Controller_loadDataBin(c)) PREPROCESS_Controller_loadDataPool(c);
    */


    return 0;
}
