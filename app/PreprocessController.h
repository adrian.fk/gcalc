//
// Created by Adrian Karlsen on 03/02/2021.
//

#ifndef GEOMATRADECRYPTER_PREPROCESSCONTROLLER_H
#define GEOMATRADECRYPTER_PREPROCESSCONTROLLER_H


#include "controller/PreprocessPresenter.h"


typedef struct ClassPreprocessController PreprocessController;


PreprocessController* PREPROCESS_Controller_new();

void PREPROCESS_Controller_init(PreprocessController* controller);

/*
 *  @return: Boolean value that signifies whether the binary loadable file exists
 *           (file with all pre-calculations already concluded.)
 */
Boolean PREPROCESS_Controller_isDataPoolAggregated(PreprocessController* controller);

/*
 *   Method to carry out the first loading of data from txt file to the savable struct
 *   @return: Boolean whether or not the loading was carried out successfully.
 */
Boolean PREPROCESS_Controller_loadDataPool(PreprocessController* controller);

/*
 *   Method to load the already precalculated binary data structure.
 *   @return: Boolean whether or not the loading was carried out successfully.
 */
Boolean PREPROCESS_Controller_loadDataBin(PreprocessController* controller);

void PREPROCESS_Controller_free(PreprocessController* controller);



#endif //GEOMATRADECRYPTER_PREPROCESSCONTROLLER_H
