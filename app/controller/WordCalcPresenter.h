//
// Created by Adrian Karlsen on 13/02/2021.
//

#ifndef GEOMATRADECRYPTER_WORDCALCPRESENTER_H
#define GEOMATRADECRYPTER_WORDCALCPRESENTER_H

#include "presenter/WordCalcConsoleView.h"
#include "WordLink.h"

typedef struct Class_WordCalcPresenter WordCalcPresenter;


WordCalcPresenter* WORDCALC_Presenter_new();

void WORDCALC_Presenter_presentInputQuery(WordCalcPresenter* presenter);

String* WORDCALC_Presenter_getInputBuf(WordCalcPresenter* presenter);

void WORDCALC_Presenter_presentResult(WordCalcPresenter* presenter, WordLink* wordLink);

void WORDCALC_Presenter_presentEnd(WordCalcPresenter* presenter);

Boolean WORDCALC_Presenter_isEnd(WordCalcPresenter* presenter);

void WORDCALC_Presenter_free(WordCalcPresenter* presenter);

#endif //GEOMATRADECRYPTER_WORDCALCPRESENTER_H
