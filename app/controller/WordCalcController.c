//
// Created by Adrian Karlsen on 13/02/2021.
//

#include <stdlib.h>
#include "../WordCalcController.h"
#include "DataLinkAdapter.h"
#include "WordLink.h"


struct Class_WordCalcController {
    WordCalcPresenter* presenter;
};


void WORDCALC_Controller_processInput(WordCalcController* controller, String *input, WordLink* wordLinkContainer);

WordCalcController* WORDCALC_Controller_new() {
    WordCalcController* controller = (WordCalcController*) malloc(sizeof(WordCalcController));
    if (controller) {
        controller->presenter = WORDCALC_Presenter_new();
    }
    return controller;
}

void WORDCALC_Controller_init(WordCalcController* controller) {
    String* inputBuf = STRING_new();
    while (controller != NULL && !WORDCALC_Presenter_isEnd(controller->presenter)) {
        WordLink* wordLink = WORDLINK_new();

        WORDCALC_Presenter_presentInputQuery(controller->presenter);
        STRING_setString(inputBuf, WORDCALC_Presenter_getInputBuf(controller->presenter));
        WORDCALC_Controller_processInput(controller, inputBuf, wordLink);
        //WORDCALC_Presenter_presentResult(controller->presenter, wordLink);
        WORDCALC_Presenter_presentEnd(controller->presenter);

        WORDLINK_free(wordLink);
    }
    STRING_destroy(inputBuf);
}

void WORDCALC_Controller_processInput(WordCalcController* controller, String *input, WordLink* wordLink) {
    if (input && wordLink) {
        int numEngOrd = DATA_LINK_ADAPTER_convertWordToEngOrdNum(input);
        int numFullRed = DATA_LINK_ADAPTER_convertWordToFullRedNum(input);
        int numRevOrd = DATA_LINK_ADAPTER_convertWordToRevOrdNum(input);
        int numRevFullRed = DATA_LINK_ADAPTER_convertWordToRevFullRedNum(input);

        WORDLINK_setNumEngOrd(wordLink, numEngOrd);
        WORDLINK_setNumFullRed(wordLink, numFullRed);
        WORDLINK_setNumRevOrd(wordLink, numRevOrd);
        WORDLINK_setNumRevFullRed(wordLink, numRevFullRed);


        if (numEngOrd > 0
            && numFullRed > 0
            && numRevOrd > 0
            && numRevFullRed > 0) {

            WORDCALC_Presenter_presentResult(controller->presenter, wordLink);
        }

        //Requires dataLinks import
        //int nuwTotalOccurrences = ;

    }

}

void WORDCALC_Controller_free(WordCalcController* controller) {
    WORDCALC_Presenter_free(controller->presenter);
    free(controller);
}
