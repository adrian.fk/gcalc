//
// Created by Adrian Karlsen on 12/02/2021.
//

#ifndef GEOMATRADECRYPTER_WORDLINK_H
#define GEOMATRADECRYPTER_WORDLINK_H

typedef struct Class_WordLinkModel WordLink;

WordLink * WORDLINK_new();

void WORDLINK_free(WordLink* wordLink);


int WORDLINK_getNumEngOrd(WordLink* wordLink);

int WORDLINK_getNumFullRed(WordLink* wordLink);

int WORDLINK_getNumRevOrd(WordLink* wordLink);

int WORDLINK_getNumRevFullRed(WordLink* wordLink);

int WORDLINK_getNumTotalOccurrences(WordLink* wordLink);

void WORDLINK_setNumEngOrd(WordLink* wordLink, int numEngOrd);

void WORDLINK_setNumFullRed(WordLink* wordLink, int numFullRed);

void WORDLINK_setNumRevOrd(WordLink* wordLink, int numRevOrd);

void WORDLINK_setNumRevFullRed(WordLink* wordLink, int numRevFullRed);

void WORDLINK_setNumTotalOccurrences(WordLink* wordLink, int numTotalOccurrences);




#endif //GEOMATRADECRYPTER_WORDLINK_H
