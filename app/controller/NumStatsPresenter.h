//
// Created by Adrian Karlsen on 12/02/2021.
//

#ifndef GEOMATRADECRYPTER_NUMSTATSPRESENTER_H
#define GEOMATRADECRYPTER_NUMSTATSPRESENTER_H

#include "presenter/NumStatsConsoleView.h"
#include "DataLinks.h"


typedef struct Class_NumStatsPresenter NumStatsPresenter;

NumStatsPresenter* NUMSTATS_Presenter_new();

void NUMSTATS_Presenter_presentNumberQuery(NumStatsPresenter* presenter, void* out);

void NUMSTATS_Presenter_presentStats(NumStatsPresenter* presenter, DataLinks* data, String* number);

int NUMSTATS_Presenter_queryNumber(NumStatsPresenter* presenter, int* out);

void NUMSTATS_Presenter_free(NumStatsPresenter* presenter);



#endif //GEOMATRADECRYPTER_NUMSTATSPRESENTER_H
