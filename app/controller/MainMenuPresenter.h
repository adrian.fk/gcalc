//
// Created by Adrian Karlsen on 03/02/2021.
//

#ifndef GEOMATRADECRYPTER_MAINMENUPRESENTER_H
#define GEOMATRADECRYPTER_MAINMENUPRESENTER_H

#include "../utils/c_utils/Utils.h"


typedef struct ClassMainMenuPresenter MainMenuPresenter;

MainMenuPresenter* MAIN_MENU_Presenter_new();

void MAIN_MENU_Presenter_configureMenu(MainMenuPresenter* presenter,
                                       void (*calibrateSysHandler),
                                       void (*numberStatsHandler),
                                       void (*calcValWord));

void MAIN_MENU_Presenter_presentMenu(MainMenuPresenter* presenter);

void MAIN_MENU_Presenter_destroy(MainMenuPresenter* presenter);

#endif //GEOMATRADECRYPTER_MAINMENUPRESENTER_H
