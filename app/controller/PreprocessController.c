//
// Created by Adrian Karlsen on 03/02/2021.
//

#include <pthread.h>
#include "../PreprocessController.h"
#include "domain/loader/Files/TxtFileLoader.h"
#include "DataLinks.h"
#include "DataLinkAdapter.h"

#define DEFAULT_NUM_THREADS_UTILISATION 8



Boolean UseMultiThreading = TRUE;

int progress_tracker = -1;



struct ClassPreprocessController {
    PreprocessPresenter* presenter;
    //Model reference pointer (Model must have a singleton)
    DataLinks* model;
};


PreprocessController* PREPROCESS_Controller_new() {
    PreprocessController* pController = (PreprocessController*) malloc(sizeof(struct ClassPreprocessController));
    if (NULL != pController) {
        pController->presenter = PREPROCESS_Presenter_new();
        pController->model = DATA_LINKS_getInstance();

        return pController;
    }
    return NULL;
}

void PREPROCESS_Controller_init(PreprocessController* controller) {
    PREPROCESS_Presenter_presentProcessBar(controller->presenter, 0);
}

/*
 *  @return: Boolean value that signifies whether the binary loadable file exists
 *           (file with all pre-calculations already concluded.)
 */
Boolean PREPROCESS_Controller_isDataPoolAggregated(PreprocessController* controller) {
    FILE* fp = fopen("data.bin", "rb");
    if (fp != NULL) {
        fclose(fp);
        return TRUE;
    }
    return FALSE;
}

void processWordBuffer(DataLinks *model, String *buffer) {
    int engOrdNum = DATA_LINK_ADAPTER_convertWordToEngOrdNum(buffer);
    if (engOrdNum < 400) {
        int fullRedNum = DATA_LINK_ADAPTER_convertWordToFullRedNum(buffer);
        int revOrdNum = DATA_LINK_ADAPTER_convertWordToRevOrdNum(buffer);
        int revFullRedNum = DATA_LINK_ADAPTER_convertWordToRevFullRedNum(buffer);

        DATA_LINKS_WordToEngOrdNum_add(model, buffer, engOrdNum);
        DATA_LINKS_WordToFullRedNum_add(model, buffer, fullRedNum);
        DATA_LINKS_WordToRevOrdNum_add(model, buffer, revOrdNum);
        DATA_LINKS_WordToRevFullRedNum_add(model, buffer, revFullRedNum);
    }
}

typedef struct {
    DataLinks* model;
    char dataSeparator;
    char *aggregateBuf;
    String *buffer;
    int i;
}args_processTxtFileCharacter;

void *processTxtFileCharacter(void* vargs) {
    args_processTxtFileCharacter* args = (args_processTxtFileCharacter*) vargs;
    char tarChar = args->aggregateBuf[args->i];
    if (tarChar == args->dataSeparator) {
        processWordBuffer(args->model, args->buffer);
        STRING_set(args->buffer, "");
    }
    else {
        STRING_appendChar(args->buffer, tarChar);
    }
    free(args);
    return NULL;
}

int getEndPointOfInputBuffer(int position, String *input) {
    char* inputCharArr = STRING_getCharArr(input);
    for (int i = position; i != 0; --i) {
        if (inputCharArr[i] == '\n') return i;
    }
    return 0;
}

typedef struct {
    PreprocessController *controller;
    DataLinks *model;
    String *txtFileBuffer;
    int* threadCursor;
    char dataSeparator;
    char *aggregateBuf;
}args_workloadHandler;

void* workloadHandler(void* vargs) {
    args_workloadHandler* args = (args_workloadHandler*) vargs;
    String* buffer = STRING_new();

    for (int i = 0; i < STRING_getLength(args->txtFileBuffer); ++i, args->threadCursor++) {
        double progressFactor = (double) i / STRING_getLength(args->txtFileBuffer);
        int progress = 100 * progressFactor;
        if (progress > progress_tracker) {
            progress_tracker = progress;
            PREPROCESS_Presenter_presentProcessBar(args->controller->presenter, progress);

        }
        args_processTxtFileCharacter* args_txtFileCharacter = (args_processTxtFileCharacter*) malloc(sizeof(args_processTxtFileCharacter));
        args_txtFileCharacter->model = args->model;
        args_txtFileCharacter->dataSeparator = args->dataSeparator;
        args_txtFileCharacter->aggregateBuf = args->aggregateBuf;
        args_txtFileCharacter->buffer = buffer;
        args_txtFileCharacter->i = i;
        
        processTxtFileCharacter(args_txtFileCharacter);
    }
    free(vargs);
    pthread_exit(NULL);
}

void waitForThreads(pthread_t pthreadContainer[], int numThreads) {
    for (int i = 0; i < numThreads; ++i) {
        pthread_join(pthreadContainer[i], NULL);
    }
}

void mapToWorkHandlerArgs(args_workloadHandler *out, DataLinks *model, PreprocessController *controller, char dataSeparator,
                     int *threadCursor, String *substringBuf, char *aggregateBuf) {
    out->controller = controller;
    out->model = model;
    out->txtFileBuffer = substringBuf;
    out->threadCursor = threadCursor;
    out->dataSeparator = dataSeparator;
    out->aggregateBuf = aggregateBuf;
}

void clearFilledSubstringBuf(int numThreads, String *const *substringBuf) {
    for (int i = 0; i < numThreads; ++i) {
        STRING_destroy(substringBuf[i]);
    }
}

void parseWordTxtFile(PreprocessController* controller, DataLinks* model, String* txtFileBuffer) {

    int numThreads = DEFAULT_NUM_THREADS_UTILISATION, threadCursor = 0;
    pthread_t pthreads[numThreads];

    char dataSeparator = '\n';

    char* aggregateBuf = STRING_getCharArr(txtFileBuffer);
    String* substringBuf[numThreads];

    if (UseMultiThreading == TRUE) {
        int lengthEachThreadInterval = STRING_getLength(txtFileBuffer) / numThreads;

        for (int i = 0, processIntervalStartPoint = 0; i < numThreads; ++i) {
            int processIntervalEndPoint = getEndPointOfInputBuffer((processIntervalStartPoint + lengthEachThreadInterval), txtFileBuffer);
            substringBuf[i] = STRING_subString_new(txtFileBuffer, processIntervalStartPoint, processIntervalEndPoint);


            args_workloadHandler* workloadHandler_args = malloc(sizeof(args_workloadHandler));

            mapToWorkHandlerArgs(workloadHandler_args, model, controller, dataSeparator, &threadCursor, substringBuf[i],
                                 aggregateBuf);

            pthread_create(&pthreads[i], NULL, workloadHandler, workloadHandler_args);

            processIntervalStartPoint = processIntervalEndPoint + 1;
        }
        waitForThreads(pthreads, numThreads);
        clearFilledSubstringBuf(numThreads, substringBuf);
    }
    else {
        args_workloadHandler* workloadHandler_args = malloc(sizeof(args_workloadHandler));

        mapToWorkHandlerArgs(workloadHandler_args, model, controller, dataSeparator, &threadCursor, txtFileBuffer,
                             aggregateBuf);

        workloadHandler(workloadHandler_args);
    }
    
}

void saveDataModel(DataLinks* model) {
    FILE* fp = fopen("data.bin", "wb");
    if (fp) {
        fwrite(model, DATA_LINKS_getSizeOfClass(), 1, fp);
        fclose(fp);
    }
}

/*
 *   Method to carry out the first loading of data from txt file to the savable struct
 *   @return: Boolean whether or not the loading was carried out successfully.
 */
Boolean PREPROCESS_Controller_loadDataPool(PreprocessController* controller) {
    String* buffer = STRING_new();
    if (!TEXTFILE_load("english-words.txt", buffer)) {
        return FALSE;
    }

    parseWordTxtFile(controller, controller->model, buffer);
    STRING_destroy(buffer);
    saveDataModel(controller->model);


    return TRUE;
}

/*
 *   Method to load the already precalculated binary data structure.
 *   @return: Boolean whether or not the loading was carried out successfully.
 */
Boolean PREPROCESS_Controller_loadDataBin(PreprocessController* controller) {
    if (PREPROCESS_Controller_isDataPoolAggregated(controller)) {
        FILE* fp = fopen("data.bin", "rb");
        if (fp) {
            fread(controller->model, DATA_LINKS_getSizeOfClass(), 1, fp);
            fclose(fp);
        }
        return FALSE;
    }
    return FALSE;
}

void PREPROCESS_Controller_free(PreprocessController* controller) {
    PREPROCESS_Presenter_free(controller->presenter);
    //We du not free the datalink singleton here
}
