//
// Created by Adrian Karlsen on 03/02/2021.
//

#include "../MainMenuController.h"
#include "../PreprocessController.h"
#include "../NumStatsController.h"
#include "../WordCalcController.h"

struct ClassMainMenuController {
    MainMenuPresenter* presenter;
};



MainMenuController* MAIN_MENU_new() {
    MainMenuController* controller = (MainMenuController*) malloc(sizeof(MainMenuController));
    if (controller) {
        controller->presenter = MAIN_MENU_Presenter_new();
    }
    return controller;
}

void MAIN_MENU_Controller_calibrateSystemMenuOptionHandler() {
    void* c = PREPROCESS_Controller_new();
    PREPROCESS_Controller_loadDataPool(c);
}

void MAIN_MENU_Controller_numberStatisticMenuOptionHandler() {
    void* c = NUMSTATS_Controller_new();
    NUMSTATS_Controller_init(c);
}

void MAIN_MENU_Controller_calcValFromWordMenuOptionHandler() {
    void* c = WORDCALC_Controller_new();
    WORDCALC_Controller_init(c);
}


void MAIN_MENU_Controller_init(MainMenuController* controller) {
    MAIN_MENU_Presenter_configureMenu(
            controller->presenter,
            &MAIN_MENU_Controller_calibrateSystemMenuOptionHandler,
            &MAIN_MENU_Controller_numberStatisticMenuOptionHandler,
            &MAIN_MENU_Controller_calcValFromWordMenuOptionHandler
    );
    MAIN_MENU_Presenter_presentMenu(controller->presenter);
}

void MAIN_MENU_Controller_destroy(MainMenuController* controller) {
    MAIN_MENU_Presenter_destroy(controller->presenter);
    free(controller);
}
