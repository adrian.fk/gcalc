//
// Created by Adrian Karlsen on 03/02/2021.
//

#ifndef GEOMATRADECRYPTER_PREPROCESSPRESENTER_H
#define GEOMATRADECRYPTER_PREPROCESSPRESENTER_H

#include "presenter/PreprocessConsoleView.h"
#include "../utils/c_utils/Utils.h"


#define PREPROCESS_PRESENTER_MODE_CONSOLE_VIEW 11



typedef struct ClassPreprocessPresenter PreprocessPresenter;

PreprocessPresenter* PREPROCESS_Presenter_new();

void PREPROCESS_Presenter_presentProcessBar(PreprocessPresenter* presenter, int percentage);

void PREPROCESS_Presenter_free(PreprocessPresenter* presenter);


#endif //GEOMATRADECRYPTER_PREPROCESSPRESENTER_H
