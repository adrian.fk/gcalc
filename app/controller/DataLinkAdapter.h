//
// Created by Adrian Karlsen on 05/02/2021.
//

#ifndef GEOMATRADECRYPTER_DATALINKADAPTER_H
#define GEOMATRADECRYPTER_DATALINKADAPTER_H

#include "../utils/c_utils/Utils.h"


int DATA_LINK_ADAPTER_convertWordToEngOrdNum(String* word);

int DATA_LINK_ADAPTER_convertWordToFullRedNum(String* word);

int DATA_LINK_ADAPTER_convertWordToRevOrdNum(String* word);

int DATA_LINK_ADAPTER_convertWordToRevFullRedNum(String* word);

#endif //GEOMATRADECRYPTER_DATALINKADAPTER_H
