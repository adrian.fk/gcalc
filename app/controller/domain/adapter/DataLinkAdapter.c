//
// Created by Adrian Karlsen on 05/02/2021.
//

#include "../../DataLinkAdapter.h"



Boolean isCharacter(char tarChar) { return (tarChar >= 'A' && tarChar <= 'z'); }

Boolean isInInterval(char tarChar, char startChar, char endChar) { return (tarChar >= startChar && tarChar <= endChar); }


int DATA_LINK_ADAPTER_convertWordToEngOrdNum(String* word) {
    int sum = 0;
    char* aux_word = STRING_getCharArr(word);
    for (int i = 0; i < STRING_getLength(word); ++i) {
        char tarChar = aux_word[i];
        int charVal = 0;
        if (tarChar >= 'A' && tarChar <= 'Z') {
            charVal = 1 + tarChar - 'A';
        }
        else if (tarChar >= 'a' && tarChar <= 'z') {
            charVal = 1 + tarChar - 'a';
        }
        sum += charVal;
    }
    return sum;
}

int DATA_LINK_ADAPTER_convertWordToFullRedNum(String* word) {
    int sum = 0;
    char* aux_word = STRING_getCharArr(word);
    for (int i = 0; i < STRING_getLength(word); ++i) {
        char tarChar = aux_word[i];
        int charVal = 0;
        if (isCharacter(tarChar)) {
            if (isInInterval(tarChar, 'A', 'I') || isInInterval(tarChar, 'a', 'i')) {
                if (isInInterval(tarChar, 'A', 'I')) {
                    charVal = 1 + tarChar - 'A';
                }
                else if (isInInterval(tarChar, 'a', 'i')) {
                    charVal = 1 + tarChar - 'a';
                }
            }
            else if (isInInterval(tarChar, 'J', 'R') || isInInterval(tarChar, 'j', 'r')) {
                if (isInInterval(tarChar, 'J', 'R')) {
                    charVal = 1 + tarChar - 'J';
                }
                else if (isInInterval(tarChar, 'j', 'r')) {
                    charVal = 1 + tarChar - 'j';
                }
            }
            else if (isInInterval(tarChar, 'S', 'Z') || isInInterval(tarChar, 's', 'z')) {
                if (isInInterval(tarChar, 'S', 'Z')) {
                    charVal = 1 + tarChar - 'S';
                }
                else if (isInInterval(tarChar, 's', 'z')) {
                    charVal = 1 + tarChar - 's';
                }
            }
            sum += charVal;
        }
    }
    return sum;
}

int DATA_LINK_ADAPTER_convertWordToRevOrdNum(String* word) {
    int sum = 0;
    char* aux_word = STRING_getCharArr(word);
    for (int i = 0; i < STRING_getLength(word); ++i) {
        char tarChar = aux_word[i];
        int charVal = 0;
        if (tarChar >= 'A' && tarChar <= 'Z') {
            charVal = 1 + ('Z' - tarChar);
        }
        else if (tarChar >= 'a' && tarChar <= 'z') {
            charVal = 1 + ('z' - tarChar);
        }
        sum += charVal;
    }
    return sum;
}

int DATA_LINK_ADAPTER_convertWordToRevFullRedNum(String* word) {
    int sum = 0;
    char* aux_word = STRING_getCharArr(word);
    for (int i = 0; i < STRING_getLength(word); ++i) {
        char tarChar = aux_word[i];
        int charVal = 0;
        if (isCharacter(tarChar)) {
            if (isInInterval(tarChar, 'A', 'H') || isInInterval(tarChar, 'a', 'h')) {
                if (isInInterval(tarChar, 'A', 'H')) {
                    charVal = 'H' - tarChar + 1;
                }
                else if (isInInterval(tarChar, 'a', 'h')) {
                    charVal = 'h' - tarChar + 1;
                }
            }
            else if (isInInterval(tarChar, 'I', 'Q') || isInInterval(tarChar, 'i', 'q')) {
                if (isInInterval(tarChar, 'J', 'Q')) {
                    charVal = 'Q' - tarChar + 1;
                }
                else if (isInInterval(tarChar, 'j', 'q')) {
                    charVal = 'q' - tarChar + 1;
                }
            }
            else if (isInInterval(tarChar, 'R', 'Z') || isInInterval(tarChar, 'r', 'z')) {
                if (isInInterval(tarChar, 'R', 'Z')) {
                    charVal = 'Z' - tarChar + 1;
                }
                else if (isInInterval(tarChar, 'r', 'z')) {
                    charVal = 'z' - tarChar + 1;
                }
            }
            sum += charVal;
        }
    }
    return sum;
}
