//
// Created by Adrian Karlsen on 03/02/2021.
//

#include "../../DataLinks.h"
#include <stdlib.h>

#define INSTANCE dataLinks_INSTANCE


void validateModel(DataLinks* model);



struct Class_DataLinks {
    int totalNumWords;

    //calculation maps
    Map* wordToEngOrdNum;
    Map* wordToFullRedNum;
    Map* wordToRevOrdNum;
    Map* wordToRevFullRedNum;

    //Key: a number // Value: total occurrences in all calculation maps
    Map* numToTotalOccurrences;
};

DataLinks* dataLinks_INSTANCE = NULL;

DataLinks* DATA_LINKS_new() {
    DataLinks* pDataLinks = (DataLinks*) malloc(sizeof(DataLinks));
    if (pDataLinks) {
        pDataLinks->totalNumWords = 0;

        pDataLinks->wordToEngOrdNum = MAP_new();
        pDataLinks->wordToFullRedNum = MAP_new();
        pDataLinks->wordToRevOrdNum = MAP_new();
        pDataLinks->wordToRevFullRedNum = MAP_new();

        pDataLinks->numToTotalOccurrences = MAP_new();


        MAP_init(pDataLinks->wordToEngOrdNum, sizeof(int));
        MAP_init(pDataLinks->wordToFullRedNum, sizeof(int));
        MAP_init(pDataLinks->wordToRevOrdNum, sizeof(int));
        MAP_init(pDataLinks->wordToRevFullRedNum, sizeof(int));

        MAP_init(pDataLinks->numToTotalOccurrences, sizeof(int));


        return pDataLinks;
    }
    return NULL;
}

DataLinks* DATA_LINKS_getInstance() {
    if (INSTANCE == NULL) {
        INSTANCE = DATA_LINKS_new();
    }
    return dataLinks_INSTANCE;
}



void DATA_LINKS_init(DataLinks* newDataLinksModel) {
    if (newDataLinksModel != NULL) {
        DATA_LINKS_free(dataLinks_INSTANCE);

        dataLinks_INSTANCE = newDataLinksModel;
    }
}

int DATA_LINKS_getTotNumWords(DataLinks* model) {
    validateModel(model);
    if (model != NULL) return INSTANCE->totalNumWords;
    return 0;
}

void incrementTotalNumOccurrences(DataLinks* model, String* numberKey) {
    validateModel(model);
    int* targetVal = (int*) MAP_get(model->numToTotalOccurrences, STRING_getCharArr(numberKey));
    if (targetVal != NULL) {
        *targetVal++;
    }
    else {
        targetVal = (int*) malloc(sizeof(int));
        *targetVal = 1;
        MAP_add_stringKey(model->numToTotalOccurrences, numberKey, targetVal);
    }
}



void incrementMapEntryItem(DataLinks* model, String* numberKey) {
    incrementTotalNumOccurrences(model, numberKey);
    model->totalNumWords++;
}

void handleTotalNumOccurrences(DataLinks* model, int val) {
    String* number = STRING_new();
    STRING_convert_intToString(number, val);
    incrementMapEntryItem(model, number);
    STRING_destroy(number);
}

void DATA_LINKS_WordToEngOrdNum_add(DataLinks* model, String* key, int value) {
    validateModel(model);
    MAP_add(model->wordToEngOrdNum, STRING_getCharArr(key), &value);
    handleTotalNumOccurrences(model, value);
}

void DATA_LINKS_WordToFullRedNum_add(DataLinks* model, String* key, int value) {
    validateModel(model);
    MAP_add(model->wordToFullRedNum, STRING_getCharArr(key), &value);
    handleTotalNumOccurrences(model, value);
}

void DATA_LINKS_WordToRevOrdNum_add(DataLinks* model, String* key, int value) {
    validateModel(model);
    MAP_add(model->wordToRevOrdNum, STRING_getCharArr(key), &value);
    handleTotalNumOccurrences(model, value);
}

void DATA_LINKS_WordToRevFullRedNum_add(DataLinks* model, String* key, int value) {
    validateModel(model);
    MAP_add(model->wordToRevFullRedNum, STRING_getCharArr(key), &value);
    handleTotalNumOccurrences(model, value);
}

void DATA_LINKS_NumToTotalOccurrences_add(DataLinks* model, String* key, int value) {
    validateModel(model);
    MAP_add(model->numToTotalOccurrences, STRING_getCharArr(key), &value);
}


int* DATA_LINKS_WordToEngOrdNum_get(DataLinks* model, String* key) {
    validateModel(model);
    return (int*) MAP_get(model->wordToEngOrdNum, STRING_getCharArr(key));
}

int* DATA_LINKS_WordToFullRedNum_get(DataLinks* model, String* key) {
    validateModel(model);
    return (int*) MAP_get(model->wordToFullRedNum, STRING_getCharArr(key));
}

int* DATA_LINKS_WordToRevOrdNum_get(DataLinks* model, String* key) {
    validateModel(model);
    return (int*) MAP_get(model->wordToRevOrdNum, STRING_getCharArr(key));
}

int* DATA_LINKS_WordToRevFullRedNum_get(DataLinks* model, String* key) {
    validateModel(model);
    return (int*) MAP_get(model->wordToRevFullRedNum, STRING_getCharArr(key));
}

int* DATA_LINKS_NumToTotalOccurrences_get(DataLinks* model, String* key) {
    validateModel(model);
    return (int*) MAP_get(model->numToTotalOccurrences, STRING_getCharArr(key));
}


Map* DATA_LINKS_WordToEngOrdNum_getMap(DataLinks* model) {
    validateModel(model);
    return model->wordToEngOrdNum;
}

Map* DATA_LINKS_WordToFullRedNum_getMap(DataLinks* model) {
    validateModel(model);
    return model->wordToFullRedNum;
}

Map* DATA_LINKS_WordToRevOrdNum_getMap(DataLinks* model) {
    validateModel(model);
    return model->wordToRevOrdNum;
}

Map* DATA_LINKS_WordToRevFullRedNum_getMap(DataLinks* model) {
    validateModel(model);
    return model->wordToRevFullRedNum;
}

Map* DATA_LINKS_NumToTotalOccurrences_getMap(DataLinks* model) {
    validateModel(model);
    return model->numToTotalOccurrences;
}


size_t DATA_LINKS_getSizeOfClass() {
    return sizeof(struct Class_DataLinks);
}

void DATA_LINKS_free(DataLinks* dataLinks) {
    validateModel(dataLinks);
    MAP_free(dataLinks->wordToEngOrdNum);
    MAP_free(dataLinks->wordToFullRedNum);
    MAP_free(dataLinks->wordToRevOrdNum);
    MAP_free(dataLinks->wordToRevFullRedNum);

    MAP_free(dataLinks->numToTotalOccurrences);
}

void validateModel(DataLinks* model) {
    if (model == NULL) {
        model = DATA_LINKS_getInstance();
    }
}
