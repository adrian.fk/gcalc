//
// Created by Adrian Karlsen on 12/02/2021.
//

#include <stdlib.h>
#include "../../WordLink.h"


struct Class_WordLinkModel {
    int numEngOrd;
    int numFullRed;
    int numRevOrd;
    int numRevFullRed;

    int nuwTotalOccurrences;
};

WordLink* WORDLINK_new() {
    WordLink* wordLink = (WordLink*) malloc(sizeof(WordLink));
    if (wordLink) {
        wordLink->numEngOrd = 0;
        wordLink->numFullRed = 0;
        wordLink->numRevOrd = 0;
        wordLink->numRevFullRed = 0;
        wordLink->nuwTotalOccurrences = 0;
    }
    return wordLink;
}

void WORDLINK_free(WordLink* wordLink) {
    free(wordLink);
}

int WORDLINK_getNumEngOrd(WordLink* wordLink) {
    if (wordLink) return wordLink->numEngOrd;
    return 0;
}

int WORDLINK_getNumFullRed(WordLink* wordLink) {
    if (wordLink) return wordLink->numFullRed;
    return 0;
}

int WORDLINK_getNumRevOrd(WordLink* wordLink) {
    if (wordLink) return wordLink->numRevOrd;
    return 0;
}

int WORDLINK_getNumRevFullRed(WordLink* wordLink) {
    if (wordLink) return wordLink->numRevFullRed;
    return 0;
}

int WORDLINK_getNumTotalOccurrences(WordLink* wordLink) {
    if (wordLink) return wordLink->nuwTotalOccurrences;
    return 0;
}

void WORDLINK_setNumEngOrd(WordLink* wordLink, int numEngOrd) {
    if (wordLink) wordLink->numEngOrd = numEngOrd;
}

void WORDLINK_setNumFullRed(WordLink* wordLink, int numFullRed) {
    if (wordLink) wordLink->numFullRed = numFullRed;
}

void WORDLINK_setNumRevOrd(WordLink* wordLink, int numRevOrd) {
    if (wordLink) wordLink->numRevOrd = numRevOrd;
}

void WORDLINK_setNumRevFullRed(WordLink* wordLink, int numRevFullRed) {
    if (wordLink) wordLink->numRevFullRed = numRevFullRed;
}

void WORDLINK_setNumTotalOccurrences(WordLink* wordLink, int numTotalOccurrences) {
    if (wordLink) wordLink->nuwTotalOccurrences = numTotalOccurrences;
}

