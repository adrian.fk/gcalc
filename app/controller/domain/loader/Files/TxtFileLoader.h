//
// Created by adria on 03/03/2020.
//


#include "../../../../utils/c_utils/Utils.h"

#ifndef P2_ADRIAN_FALCH_TXTFILELOADER_H
#define P2_ADRIAN_FALCH_TXTFILELOADER_H

#endif //P2_ADRIAN_FALCH_TXTFILELOADER_H

/*
 *  @return: Boolean value representing whether the file coule be successfully loaded or not.
 */
int TEXTFILE_load(char* filePath, String* _dest);