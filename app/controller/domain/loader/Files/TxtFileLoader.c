//
// Created by adria on 03/03/2020.
//

#include <stdlib.h>
#include <stdio.h>
#include "TxtFileLoader.h"

#define BUFFER_SIZE 1024

Boolean TEXTFILE_load(char* filePath, String* _dest) {
    char buffer[BUFFER_SIZE] = {'\0'};
    FILE* fp = fopen(filePath, "r");
    if(!fp) {
        return FALSE;
    }
    else {
        fgets(buffer, BUFFER_SIZE, fp);

        do {
            STRING_append(_dest, buffer);
            fgets(buffer, BUFFER_SIZE, fp);
        }while(!feof(fp));
        fclose(fp);
    }
    return TRUE;
}