//
// Created by Adrian Karlsen on 12/02/2021.
//

#include "../NumStatsController.h"

struct Class_NumStatsController {
    NumStatsPresenter* presenter;
    DataLinks* model;
};

NumStatsController* NUMSTATS_Controller_new() {
    NumStatsController* controller = (NumStatsController*) malloc(sizeof(NumStatsController));
    if (controller) {
        controller->presenter = NUMSTATS_Presenter_new();
        controller->model = DATA_LINKS_getInstance();
    }
    return controller;
}

void NUMSTATS_Controller_init(NumStatsController* controller) {
    int numberInput = 0;
    String* numInputString;
    NUMSTATS_Presenter_presentNumberQuery(controller->presenter, &numberInput);
    STRING_convert_intToString(numInputString, numberInput);
    NUMSTATS_Presenter_presentStats(controller->presenter, controller->model, numInputString);
}

