//
// Created by Adrian Karlsen on 03/02/2021.
//

#ifndef GEOMATRADECRYPTER_DATALINKS_H
#define GEOMATRADECRYPTER_DATALINKS_H

#include "../utils/c_utils/Utils.h"


typedef struct Class_DataLinks DataLinks;


DataLinks* DATA_LINKS_getInstance();

void DATA_LINKS_init(DataLinks* newDataLinksModel);

int DATA_LINKS_getTotNumWords(DataLinks* model);


void DATA_LINKS_WordToEngOrdNum_add(DataLinks* model, String* key, int value);

void DATA_LINKS_WordToFullRedNum_add(DataLinks* model, String* key, int value);

void DATA_LINKS_WordToRevOrdNum_add(DataLinks* model, String* key, int value);

void DATA_LINKS_WordToRevFullRedNum_add(DataLinks* model, String* key, int value);

void DATA_LINKS_NumToTotalOccurrences_add(DataLinks* model, String* key, int value);


int* DATA_LINKS_WordToEngOrdNum_get(DataLinks* model, String* key);

int* DATA_LINKS_WordToFullRedNum_get(DataLinks* model, String* key);

int* DATA_LINKS_WordToRevOrdNum_get(DataLinks* model, String* key);

int* DATA_LINKS_WordToRevFullRedNum_get(DataLinks* model, String* key);

int* DATA_LINKS_NumToTotalOccurrences_get(DataLinks* model, String* key);


Map* DATA_LINKS_WordToEngOrdNum_getMap(DataLinks* model);

Map* DATA_LINKS_WordToFullRedNum_getMap(DataLinks* model);

Map* DATA_LINKS_WordToRevOrdNum_getMap(DataLinks* model);

Map* DATA_LINKS_WordToRevFullRedNum_getMap(DataLinks* model);

Map* DATA_LINKS_NumToTotalOccurrences_getMap(DataLinks* model);


size_t DATA_LINKS_getSizeOfClass();

void DATA_LINKS_free(DataLinks* dataLinks);

#endif //GEOMATRADECRYPTER_DATALINKS_H
