//
// Created by Adrian Karlsen on 11/02/2021.
//

#include "../NumStatsConsoleView.h"

#define CHAR_NEW_LINE '\n'


#define printHeader(view) NUMSTATS_ConsoleView_displayHeader(view)

const int VIEW_WIDTH = 50;


struct ClassNumStatsConsoleView {
    String* input;
    String* header;
};

NumStatsConsoleView* NUMSTATS_ConsoleView_new() {
    NumStatsConsoleView* v = (NumStatsConsoleView*) malloc(sizeof(NumStatsConsoleView));
    if (v != NULL) {
        v->input = STRING_new();
        v->header = STRING_new();
    }
    return v;
}

String* NUMSTATS_ConsoleView_getInput(NumStatsConsoleView* view_NOTNUL) {
    if (view_NOTNUL) {
        return view_NOTNUL->input;
    }
    return NULL;
}

int NUMSTATS_ConsoleView_queryInput(NumStatsConsoleView* NOTNULL_view, String* NULLABLE_queryMsg) {
    if (NOTNULL_view) {
        clearView;
        printHeader(NOTNULL_view);
        newLine;
        newLine;
        printCharArr("\t> ");
        if (NULLABLE_queryMsg != NULL) printStr(NULLABLE_queryMsg);
        STRING_getInput(NOTNULL_view->input);
        newLine;
        if (NOTNULL_view->input != NULL) {
            return STRING_atoi(NOTNULL_view->input);
        }
    }
    return ERROR_NO_INPUT;
}

int NUMSTATS_ConsoleView_displayAndQuery(NumStatsConsoleView* view, String* msg, int* out) {
    return NUMSTATS_ConsoleView_queryInput(view, msg);
}

void NUMSTATS_ConsoleView_displayHeader(NumStatsConsoleView* view) {
    int headerTitlePadding = (VIEW_WIDTH - STRING_getLength(view->header) - 2) / 2;

    if (view != NULL && view->header != NULL) {
        newLine;
        doPrintTimes(VIEW_WIDTH, "_");
        newLine;
        printCharArr("|");
        doPrintTimes(headerTitlePadding, " ");
        printStr(view->header);
        doPrintTimes(headerTitlePadding, " ");
        printCharArr(" |");
        newLine;
        doPrintTimes(VIEW_WIDTH, "_");
    }
}

void NUMSTATS_ConsoleView_setHeader(NumStatsConsoleView* view, String* header) {
    if (view != NULL && header != NULL) STRING_setString(view->header, header);
}

void NUMSTATS_ConsoleView_displayNumber(NumStatsConsoleView* view, String* number) {
    if (NULL != number) {
        int padding = VIEW_WIDTH - STRING_getLength(number);
        int paddingSides = padding / 2;

        newLine;
        doPrintTimes(paddingSides, "-");
        printStr(number);
        doPrintTimes(paddingSides, "-");
    }
}

void NUMSTATS_ConsoleView_displayMsg(String* msg) {
    newLine;
    printCharArr("\t > ");
    printStr(msg);
}

void NUMSTATS_ConsoleView_displayErrorMsg(String* errMsg) {
    newLine;
    printCharArr("\t > ERROR");
    if (errMsg) {
        printCharArr(" : ");
        printStr(errMsg);
    }
}

void NUMSTATS_ConsoleView_clearView() {
    clearConsoleView();
}

void NUMSTATS_ConsoleView_free(NumStatsConsoleView* view) {
    STRING_destroy(view->header);
    STRING_destroy(view->input);
    free(view);
}
