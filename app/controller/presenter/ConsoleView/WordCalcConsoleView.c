//
// Created by Adrian Karlsen on 13/02/2021.
//

#include "../WordCalcConsoleView.h"


#define VIEW_WIDTH 50


struct Class_WordCalcConsoleView {
    String* inputBuf;
    String* separatorChar;
};

WordCalcConsoleView* WORDCALC_ConsoleView_new() {
    WordCalcConsoleView* view = (WordCalcConsoleView*) malloc(sizeof(WordCalcConsoleView));
    if (view) {
        view->inputBuf = STRING_new();

        view->separatorChar = STRING_init("-");
    }
    return view;
}

char* getSeparatorChar(WordCalcConsoleView* view) {
    return STRING_getCharArr(view->separatorChar);
}

void WORDCALC_ConsoleView_displaySeparator(WordCalcConsoleView* view) {
    if (view) {
        newLine;
        doPrintTimes(VIEW_WIDTH, getSeparatorChar(view));
        newLine;
    }
}

void WORDCALC_ConsoleView_displayQuery(WordCalcConsoleView* view, String* queryMsg) {
    if (view) {
        newLine;
        newLine;
        printCharArr("\t> ");
        if (queryMsg) printStr(queryMsg);
        STRING_getInput(view->inputBuf);
    }
}

void WORDCALC_ConsoleView_displayCalculations(WordCalcConsoleView* view, WordCalcViewModel* viewModel) {
    int numEngOrd = WORDCALC_ViewModel_getNumEngOrd(viewModel);
    int numFullRed = WORDCALC_ViewModel_getNumFullRed(viewModel);
    int numRevOrd = WORDCALC_ViewModel_getNumRevOrd(viewModel);
    int numRevFullRed = WORDCALC_ViewModel_getNumRevFullRed(viewModel);

    String* numEngOrdStr = STRING_new();
    String* numFullRedStr = STRING_new();
    String* numRevOrdStr = STRING_new();
    String* numRevFullRedStr = STRING_new();

    STRING_convert_intToString(numEngOrdStr, numEngOrd);
    STRING_convert_intToString(numFullRedStr, numFullRed);
    STRING_convert_intToString(numRevOrdStr, numRevOrd);
    STRING_convert_intToString(numRevFullRedStr, numRevFullRed);


    newLine;
    printCharArr("English Ordinal: ");
    printStr(numEngOrdStr);
    newLine;
    printCharArr("Full Reduction: ");
    printStr(numFullRedStr);
    newLine;
    printCharArr("Reverse Ordinal: ");
    printStr(numRevOrdStr);
    newLine;
    printCharArr("Reverse Full Reduction: ");
    printStr(numRevFullRedStr);
    newLine;


    STRING_destroy(numEngOrdStr);
    STRING_destroy(numFullRedStr);
    STRING_destroy(numRevOrdStr);
    STRING_destroy(numRevFullRedStr);
}

void WORDCALC_ConsoleView_getInputBuffer(WordCalcConsoleView* view, String* out) {
    if (view) STRING_setString(out, view->inputBuf);
}

void WORDCALC_ConsoleView_displayError(WordCalcConsoleView* view, String* errMsg) {
    if (view) {
        newLine;
        if (errMsg) {
            printCharArr("ERROR : ");
            printStr(errMsg);
        }
        else {
            printCharArr("ERROR");
        }
        newLine;
    }
}

void WORDCALC_ConsoleView_free(WordCalcConsoleView* view) {
    STRING_destroy(view->inputBuf);
    STRING_destroy(view->separatorChar);
    free(view);
}
