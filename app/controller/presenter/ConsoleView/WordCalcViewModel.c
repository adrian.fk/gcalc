//
// Created by Adrian Karlsen on 12/02/2021.
//

#include "../WordCalcViewModel.h"
#include <stdio.h>
#include <stdlib.h>


struct ClassWordCalcViewModel {
    int numEngOrd;
    int numFullRed;
    int numRevOrd;
    int numRevFullRed;

    int nuwTotalOccurrences;
};


WordCalcViewModel* WORDCALC_ViewModel_new() {
    WordCalcViewModel* vm = (WordCalcViewModel*) malloc(sizeof(WordCalcViewModel));
    if (vm) {
        vm->numEngOrd = 0;
        vm->numFullRed = 0;
        vm->numRevOrd = 0;
        vm->numRevFullRed = 0;
        vm->nuwTotalOccurrences = 0;
    }
    return vm;
}

void WORDCALC_ViewModel_free(WordCalcViewModel* vm) {
    if (vm) free(vm);
}

int WORDCALC_ViewModel_getNumEngOrd(WordCalcViewModel* vm) {
    if (vm) return vm->numEngOrd;
    return 0;
}

int WORDCALC_ViewModel_getNumFullRed(WordCalcViewModel* vm) {
    if (vm) return vm->numFullRed;
    return 0;
}

int WORDCALC_ViewModel_getNumRevOrd(WordCalcViewModel* vm) {
    if (vm) return vm->numRevOrd;
    return 0;
}

int WORDCALC_ViewModel_getNumRevFullRed(WordCalcViewModel* vm) {
    if (vm) return vm->numRevFullRed;
    return 0;
}

int WORDCALC_ViewModel_getNumTotalOccurrences(WordCalcViewModel* vm) {
    if (vm) return vm->nuwTotalOccurrences;
    return 0;
}

void WORDCALC_ViewModel_setNumEngOrd(WordCalcViewModel* vm, int numEngOrd) {
    if (vm) vm->numEngOrd = numEngOrd;
}

void WORDCALC_ViewModel_setNumFullRed(WordCalcViewModel* vm, int numFullRed) {
    if (vm) vm->numFullRed = numFullRed;
}

void WORDCALC_ViewModel_setNumRevOrd(WordCalcViewModel* vm, int numRevOrd) {
    if (vm) vm->numRevOrd = numRevOrd;
}

void WORDCALC_ViewModel_setNumRevFullRed(WordCalcViewModel* vm, int numRevFullRed) {
    if (vm) vm->numRevFullRed = numRevFullRed;
}

void WORDCALC_ViewModel_setNumTotalOccurrences(WordCalcViewModel* vm, int numTotalOccurrences) {
    if (vm) vm->nuwTotalOccurrences = numTotalOccurrences;
}

