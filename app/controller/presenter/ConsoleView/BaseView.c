//
// Created by Adrian Karlsen on 12/02/2021.
//

#include "../BaseView.h"

#define NEW_LINE_STRING "\n"


void doPrintTimes(int times, char* string) {
    for (int i = 0; i < times; ++i) {
        printf("%s", string);
    }
    fflush(stdout);
}

void clearConsoleView() {
    doPrintTimes(100, NEW_LINE_STRING);
}

void displayCharArr(char* string) {
    printf("%s", string);
}

void displayString(String* string) {
    printf("%s", STRING_getCharArr(string));
}
