//
// Created by Adrian Karlsen on 04/02/2021.
//

#include <stdlib.h>
#include <stdio.h>
#include "../PreprocessConsoleView.h"

#define NEW_LINE_STRING "\n"

struct ClassPreprocessConsoleView {

};



PreprocessConsoleView* PREPROCESS_ConsoleView_new() {
    PreprocessConsoleView* pView = (PreprocessConsoleView*) malloc(sizeof(PreprocessConsoleView));
    if (pView != NULL) {

        return pView;
    }
    return NULL;
}

void PREPROCESS_ConsoleView_displayPercentageBar(PreprocessConsoleView* view, int statusPercentage) {
    printf("| Loading...");
    doPrintTimes(90, " ");
    printf(" |");
    doPrintTimes(1, NEW_LINE_STRING);
    printf("| ");
    doPrintTimes(statusPercentage, "#");
    doPrintTimes(100-statusPercentage, " ");
    printf(" |");
    fflush(stdout);
}


void PREPROCESS_ConsoleView_clear() {
    clearConsoleView();
}

void PREPROCESS_ConsoleView_free(PreprocessConsoleView* view) {
    free(view);
}


