//
// Created by Adrian Karlsen on 13/02/2021.
//

#include "../WordCalcPresenter.h"

struct Class_WordCalcPresenter {
    WordCalcConsoleView* consoleView;
    String* inputStr;
    Boolean isEnd;
};

void WORDCALC_ConsoleView_evaluateEnd(WordCalcPresenter *presenter, String* repStatement, String* exitStatement);

WordCalcPresenter* WORDCALC_Presenter_new() {
    WordCalcPresenter* presenter = (WordCalcPresenter*) malloc(sizeof(WordCalcPresenter));
    if (presenter != NULL) {
        presenter->consoleView = WORDCALC_ConsoleView_new();
        presenter->inputStr = STRING_new();
        presenter->isEnd = FALSE;
    }
    return presenter;
}

void WORDCALC_Presenter_end(WordCalcPresenter* presenter) {
    if (presenter) presenter->isEnd = TRUE;
}

void WORDCALC_Presenter_presentInputQuery(WordCalcPresenter* presenter) {
    String* queryMsg = STRING_init("Enter word: ");

    WORDCALC_ConsoleView_displayQuery(presenter->consoleView, queryMsg);
    WORDCALC_ConsoleView_getInputBuffer(presenter->consoleView, presenter->inputStr);

    STRING_destroy(queryMsg);
}

String* WORDCALC_Presenter_getInputBuf(WordCalcPresenter* presenter) {
    if (presenter) return presenter->inputStr;
    return NULL;
}

void WORDCALC_Presenter_presentResult(WordCalcPresenter* presenter, WordLink* wordLink) {
    WORDCALC_ConsoleView_displaySeparator(presenter->consoleView);

    int numEngOrd = WORDLINK_getNumEngOrd(wordLink);
    int numFullRed = WORDLINK_getNumFullRed(wordLink);
    int numRevOrd = WORDLINK_getNumRevOrd(wordLink);
    int numRevFullRed = WORDLINK_getNumRevFullRed(wordLink);

    WordCalcViewModel* wordCalcViewModel = WORDCALC_ViewModel_new();
    if (wordCalcViewModel) {
        WORDCALC_ViewModel_setNumEngOrd(wordCalcViewModel, numEngOrd);
        WORDCALC_ViewModel_setNumFullRed(wordCalcViewModel, numFullRed);
        WORDCALC_ViewModel_setNumRevOrd(wordCalcViewModel, numRevOrd);
        WORDCALC_ViewModel_setNumRevFullRed(wordCalcViewModel, numRevFullRed);

        WORDCALC_ConsoleView_displayCalculations(presenter->consoleView, wordCalcViewModel);

        WORDCALC_ViewModel_free(wordCalcViewModel);
    }
}

void WORDCALC_Presenter_presentEnd(WordCalcPresenter* presenter) {
    String* repeatSearchQueryMsg = STRING_init("Compute another word? (Y/N)");
    String* exitStatement = STRING_init("N");
    String* repeatStatement = STRING_init("Y");


    WORDCALC_ConsoleView_displaySeparator(presenter->consoleView);
    WORDCALC_ConsoleView_displayQuery(presenter->consoleView, repeatSearchQueryMsg);
    WORDCALC_ConsoleView_getInputBuffer(presenter->consoleView, presenter->inputStr);

    WORDCALC_ConsoleView_evaluateEnd(presenter, repeatStatement, exitStatement);

    STRING_destroy(repeatSearchQueryMsg);
    STRING_destroy(exitStatement);
    STRING_destroy(repeatStatement);
}

void WORDCALC_ConsoleView_evaluateEnd(WordCalcPresenter *presenter, String* repeatStatement, String* exitStatement) {
    if (presenter) {
        if (STRING_equals(presenter->inputStr, exitStatement)) {
            WORDCALC_Presenter_end(presenter);
        }
        else {
            if (STRING_equals(presenter->inputStr, repeatStatement)) {
                //Do nothing
            }
            else {
                //Handle error
                String* errMsg = STRING_init("Invalid input! Only Y and N accepted.");
                WORDCALC_ConsoleView_displayError(presenter->consoleView, errMsg);
                WORDCALC_Presenter_presentEnd(presenter); //Print error then ask again
            }
        }
    }
}

Boolean WORDCALC_Presenter_isEnd(WordCalcPresenter* presenter) {
    if (presenter) return presenter->isEnd;
    return FALSE;
}

void WORDCALC_Presenter_free(WordCalcPresenter* presenter) {
    WORDCALC_ConsoleView_free(presenter->consoleView);
    STRING_destroy(presenter->inputStr);
    free(presenter);
}


