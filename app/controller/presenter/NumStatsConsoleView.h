//
// Created by Adrian Karlsen on 11/02/2021.
//

#ifndef GEOMATRADECRYPTER_NUMSTATSCONSOLEVIEW_H
#define GEOMATRADECRYPTER_NUMSTATSCONSOLEVIEW_H

#include "../../utils/c_utils/Utils.h"
#include "BaseView.h"


#define ERROR_MSG_NUM_OCCUR_WORD_NOT_FOUND "Could not locate word in parsed dictionary.. sure word exist?"


#define ERROR_NO_INPUT 2445


typedef struct ClassNumStatsConsoleView NumStatsConsoleView;


NumStatsConsoleView* NUMSTATS_ConsoleView_new();

String* NUMSTATS_ConsoleView_getInput(NumStatsConsoleView* view);

int NUMSTATS_ConsoleView_queryInput(NumStatsConsoleView* NOTNULL_view, String* NULLABLE_queryMsg);

int NUMSTATS_ConsoleView_displayAndQuery(NumStatsConsoleView* view, String* msg, int* out);

void NUMSTATS_ConsoleView_displayHeader(NumStatsConsoleView* view);

void NUMSTATS_ConsoleView_setHeader(NumStatsConsoleView* view, String* header);

void NUMSTATS_ConsoleView_displayNumber(NumStatsConsoleView* view, String* number);

void NUMSTATS_ConsoleView_displayMsg(String* msg);

void NUMSTATS_ConsoleView_displayErrorMsg(String* errMsg);

void NUMSTATS_ConsoleView_clearView();

void NUMSTATS_ConsoleView_free(NumStatsConsoleView* view);


#endif //GEOMATRADECRYPTER_NUMSTATSCONSOLEVIEW_H
