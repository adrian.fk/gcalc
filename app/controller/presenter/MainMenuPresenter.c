//
// Created by Adrian Karlsen on 03/02/2021.
//

#include <stdlib.h>
#include "../MainMenuPresenter.h"


struct ClassMainMenuPresenter {
    Bool configured;
};


MainMenuPresenter* MAIN_MENU_Presenter_new() {
    struct ClassMainMenuPresenter* presenter = (struct ClassMainMenuPresenter*) malloc(sizeof(struct ClassMainMenuPresenter));
    if (NULL != presenter) {
        presenter->configured = FALSE;
        SIMPLE_MENU_init();

        return presenter;
    }
    return NULL;
}

void MAIN_MENU_Presenter_configureMenu(MainMenuPresenter* presenter,
                                       void (*calibrateSysHandler),
                                       void (*numberStatsHandler),
                                       void (*calcValWord)) {
    SIMPLE_MENU_createMenuItem(
            "1",
            "Calibrate system",
            calibrateSysHandler
    );
    SIMPLE_MENU_createMenuItem(
            "2",
            "Number statistics",
            numberStatsHandler
    );
    SIMPLE_MENU_createMenuItem(
            "3",
            "Calculate value of word",
            calcValWord
    );

    presenter->configured = TRUE;
}

void MAIN_MENU_Presenter_presentMenu(MainMenuPresenter* presenter) {
    if (presenter->configured) {
        SIMPLE_MENU_commit(TRUE);
    }
}

void MAIN_MENU_Presenter_destroy(MainMenuPresenter* presenter) {
    free(presenter);
}
