//
// Created by Adrian Karlsen on 12/02/2021.
//

#include "../NumStatsPresenter.h"

struct Class_NumStatsPresenter {
    NumStatsConsoleView* consoleView;
};

NumStatsPresenter* NUMSTATS_Presenter_new() {
    NumStatsPresenter* presenter = (NumStatsPresenter*) malloc(sizeof(NumStatsPresenter));

    if (presenter) {
        presenter->consoleView = NUMSTATS_ConsoleView_new();

        String* headerTitle = STRING_init("Number Statistics");
        NUMSTATS_ConsoleView_setHeader(presenter->consoleView, headerTitle);
    }

                                                            
    return presenter;
}

int NUMSTATS_Presenter_queryNumber(NumStatsPresenter* presenter, int* out) {
    //Clear view?
    int number = NUMSTATS_ConsoleView_queryInput(presenter->consoleView, NULL);
    if (NULL != out) *out = number;
    return number;
}

void NUMSTATS_Presenter_presentNumberQuery(NumStatsPresenter* presenter, void* out) {
    //Clear view?
    NUMSTATS_ConsoleView_displayHeader(presenter->consoleView);
    String* queryMsg = STRING_init("Number: ");
    int number = NUMSTATS_ConsoleView_displayAndQuery(presenter->consoleView, queryMsg, NULL);
    if (NULL != out) {
        int* intOut = (int*) out;
        *intOut = number;
        out = intOut;
    }
}

void NUMSTATS_Presenter_presentStats(NumStatsPresenter* presenter, DataLinks* data, String* number) {
    //Clear view?
    NUMSTATS_ConsoleView_displayHeader(presenter->consoleView);
    NUMSTATS_ConsoleView_displayNumber(presenter->consoleView, number);

    int* numOccurrences = (int*) DATA_LINKS_NumToTotalOccurrences_get(data, number);
    if (numOccurrences != NULL) {
        //Clear View and Print header over again?
        String* buffer = STRING_new();

        NUMSTATS_ConsoleView_clearView();
        NUMSTATS_ConsoleView_displayHeader(presenter->consoleView);

        String* numOccurDisplayString = STRING_init("Number of occurrences: ");
        String* numTotalWords = STRING_init("Out of a total of ");

        STRING_convert_intToString(buffer, *numOccurrences);
        STRING_append(numOccurDisplayString, STRING_getCharArr(buffer));

        STRING_convert_intToString(buffer, DATA_LINKS_getTotNumWords(data));
        STRING_append(numTotalWords, STRING_getCharArr(buffer));
        STRING_append(numTotalWords, " words.");

        NUMSTATS_ConsoleView_displayMsg(numOccurDisplayString);
        NUMSTATS_ConsoleView_displayMsg(numTotalWords);

        STRING_destroy(numTotalWords);
        STRING_destroy(numOccurDisplayString);
        STRING_destroy(buffer);
    }
    else {
        //Handle error - No occurrences parsed, word doesnt exist
        String* errorMsg = STRING_init(ERROR_MSG_NUM_OCCUR_WORD_NOT_FOUND);
        NUMSTATS_ConsoleView_displayErrorMsg(errorMsg);
        STRING_destroy(errorMsg);
    }
    //Present footer saying something like "Type any key to go back"
    //Wait for input


}

void NUMSTATS_Presenter_free(NumStatsPresenter* presenter) {
    NUMSTATS_ConsoleView_free(presenter->consoleView);
    free(presenter);
}

