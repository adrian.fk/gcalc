//
// Created by Adrian Karlsen on 12/02/2021.
//

#ifndef GEOMATRADECRYPTER_BASEVIEW_H
#define GEOMATRADECRYPTER_BASEVIEW_H

#include <stdio.h>
#include "../../utils/c_utils/Utils.h"


#define newLine printf("\n")
#define printStr(string) printf("%s", STRING_getCharArr(string))
#define printCharArr(charArr) printf("%s", charArr)

#define clearView clearConsoleView()

void clearConsoleView();

void doPrintTimes(int times, char* string);

void displayCharArr(char* string);

void displayString(String* string);

#endif //GEOMATRADECRYPTER_BASEVIEW_H
