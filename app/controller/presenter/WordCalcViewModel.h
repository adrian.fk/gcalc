//
// Created by Adrian Karlsen on 12/02/2021.
//

#ifndef GEOMATRADECRYPTER_WORDCALCVIEWMODEL_H
#define GEOMATRADECRYPTER_WORDCALCVIEWMODEL_H

typedef struct ClassWordCalcViewModel WordCalcViewModel;

WordCalcViewModel* WORDCALC_ViewModel_new();

void WORDCALC_ViewModel_free(WordCalcViewModel* vm);


int WORDCALC_ViewModel_getNumEngOrd(WordCalcViewModel* vm);

int WORDCALC_ViewModel_getNumFullRed(WordCalcViewModel* vm);

int WORDCALC_ViewModel_getNumRevOrd(WordCalcViewModel* vm);

int WORDCALC_ViewModel_getNumRevFullRed(WordCalcViewModel* vm);

int WORDCALC_ViewModel_getNumTotalOccurrences(WordCalcViewModel* vm);

void WORDCALC_ViewModel_setNumEngOrd(WordCalcViewModel* vm, int numEngOrd);

void WORDCALC_ViewModel_setNumFullRed(WordCalcViewModel* vm, int numFullRed);

void WORDCALC_ViewModel_setNumRevOrd(WordCalcViewModel* vm, int numRevOrd);

void WORDCALC_ViewModel_setNumRevFullRed(WordCalcViewModel* vm, int numRevFullRed);

void WORDCALC_ViewModel_setNumTotalOccurrences(WordCalcViewModel* vm, int numTotalOccurrences);




#endif //GEOMATRADECRYPTER_WORDCALCVIEWMODEL_H
