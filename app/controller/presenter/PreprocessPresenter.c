//
// Created by Adrian Karlsen on 03/02/2021.
//

#include "../PreprocessPresenter.h"

#define PREPROCESS_PRESENTER_MODE_DEFAULT PREPROCESS_PRESENTER_MODE_CONSOLE_VIEW

struct ClassPreprocessPresenter {
    int presentationMode;
    PreprocessConsoleView* consoleView;
};


PreprocessPresenter* PREPROCESS_Presenter_new() {
    PreprocessPresenter* pPresenter = (PreprocessPresenter*) malloc(sizeof(PreprocessPresenter));
    if (NULL != pPresenter) {
        pPresenter->presentationMode = PREPROCESS_PRESENTER_MODE_DEFAULT;
        pPresenter->consoleView = PREPROCESS_ConsoleView_new();

        return pPresenter;
    }

    return NULL;
}

void presentConsoleViewStatusBar(PreprocessPresenter* presenter, int percentage) {
    PREPROCESS_ConsoleView_clear();
    PREPROCESS_ConsoleView_displayPercentageBar(presenter->consoleView, percentage);
}

void PREPROCESS_Presenter_presentProcessBar(PreprocessPresenter* presenter, int percentage) {
    switch (presenter->presentationMode) {
        case PREPROCESS_PRESENTER_MODE_CONSOLE_VIEW:
            presentConsoleViewStatusBar(presenter, percentage);
            break;
    }
}

void PREPROCESS_Presenter_free(PreprocessPresenter* presenter) {
    PREPROCESS_ConsoleView_free(presenter->consoleView);
    free(presenter);
}
