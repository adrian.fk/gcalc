//
// Created by Adrian Karlsen on 04/02/2021.
//

#ifndef GEOMATRADECRYPTER_PREPROCESSCONSOLEVIEW_H
#define GEOMATRADECRYPTER_PREPROCESSCONSOLEVIEW_H

#include "BaseView.h"


typedef struct ClassPreprocessConsoleView PreprocessConsoleView;

PreprocessConsoleView* PREPROCESS_ConsoleView_new();

void PREPROCESS_ConsoleView_displayPercentageBar(PreprocessConsoleView* view, int statusPercentage);

void PREPROCESS_ConsoleView_clear();

void PREPROCESS_ConsoleView_free(PreprocessConsoleView* view);


#endif //GEOMATRADECRYPTER_PREPROCESSCONSOLEVIEW_H
