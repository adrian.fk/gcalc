//
// Created by Adrian Karlsen on 13/02/2021.
//

#ifndef GEOMATRADECRYPTER_WORDCALCCONSOLEVIEW_H
#define GEOMATRADECRYPTER_WORDCALCCONSOLEVIEW_H

#include "BaseView.h"
#include "../../utils/c_utils/Utils.h"
#include "WordCalcViewModel.h"

typedef struct Class_WordCalcConsoleView WordCalcConsoleView;

WordCalcConsoleView* WORDCALC_ConsoleView_new();

void WORDCALC_ConsoleView_displaySeparator(WordCalcConsoleView* view);

void WORDCALC_ConsoleView_displayQuery(WordCalcConsoleView* view, String* queryMsg);

void WORDCALC_ConsoleView_displayCalculations(WordCalcConsoleView* view, WordCalcViewModel* viewModel);

void WORDCALC_ConsoleView_getInputBuffer(WordCalcConsoleView* view, String* out);

void WORDCALC_ConsoleView_displayError(WordCalcConsoleView* view, String* errMsg);

void WORDCALC_ConsoleView_free(WordCalcConsoleView* view);

#endif //GEOMATRADECRYPTER_WORDCALCCONSOLEVIEW_H
