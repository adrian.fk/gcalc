//
// Created by Adrian Karlsen on 03/02/2021.
//

#ifndef GEOMATRADECRYPTER_MAINMENUCONTROLLER_H
#define GEOMATRADECRYPTER_MAINMENUCONTROLLER_H

#include "controller/MainMenuPresenter.h"


typedef struct ClassMainMenuController MainMenuController;


MainMenuController* MAIN_MENU_new();

void MAIN_MENU_Controller_init(MainMenuController* controller);

void MAIN_MENU_Controller_destroy(MainMenuController* controller);


#endif //GEOMATRADECRYPTER_MAINMENUCONTROLLER_H
